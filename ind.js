const hamburger = document.querySelector(".hamburger");
const close = document.querySelector(".close");
const modal = document.querySelector(".modal");
const mockups = document.querySelector(".Introduction .mockups");
const header = document.querySelector(".header");
const screenSize=window.matchMedia("(min-width:700px)")

screenSize.addEventListener("change",icon)

function icon(e){
 if(screenSize.matches){
    hamburger.style.display="none"
    close.style.display="none"
    document.querySelector(".Introduction").style.opacity = "1";
    mockups.style.display = "block";
    modal.style.display='none'
}
 else{
    hamburger.style.display="block"
 }
}

hamburger.addEventListener("click", displayNav);
close.addEventListener("click", closeNav);

function displayNav(e) {
  modal.style.display = "flex";
  close.style.display = "block";
  hamburger.style.display = "none";
  mockups.style.display = "none";
  document.querySelector(".Introduction").style.opacity = "0.8";
}

function closeNav(e) {
  modal.style.display = "none";
  mockups.style.display = "block";
  hamburger.style.display = "block";
  document.querySelector(".Introduction").style.opacity = "1";
  header.style.backgroundColor = "white";
  close.style.display = "none";
}
